import express from 'express'
import cors from 'cors'

const app = express()
app.use(cors())
app.get('/skb2A', function (req, res) {
	res.send(((+req.query.a | 0) + (+req.query.b | 0)).toString())
})

app.get('/skb2B', function (req, res) {
	const fullName = req.query.fullname
	var arr = fullName.split(' ')
	var result
	console.log(arr.length)
	switch (arr.length) {
		case 1:
			result = arr[0]
			break
		case 2:
			result = `${arr[1]} ${arr[0].substring(0, 1)}.`
			break
		case 3:
			result = `${arr[2]} ${arr[0].substring(0, 1)}. ${arr[1].substring(0, 1)}.`
			break
		default:
			result = "Invalid fullname"
	}
	res.send(result)
})

app.get('/skb2C', function (req, res) {
	const re = new RegExp('(https:|http:)?(//)?([\\w.-]+/)?([\\w.@]+)')
	const matchArr = req.query.username.match(re)
	var result
	if (matchArr.length == 1) {
		result = matchArr[0]
	} else if (matchArr.length > 1) {
		var arr = matchArr.filter(function(name) {
			if (typeof name == 'undefined') return false
			return name.search(new RegExp('[\-=:/]')) == -1
		})
		if (arr.length > 0) result = arr[0]
	}
	res.send(`@${result.replace('@','')}`)
})

app.listen(3000, function(){
	console.log(`Listen`)
});